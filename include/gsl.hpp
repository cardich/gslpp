// This file is part of gsl++
// (c) Apostol Faliagas, 2018

#ifndef GSL_CPP_HPP_INCLUDED
#define GSL_CPP_HPP_INCLUDED

#include <exception>
#include <functional>
#include <gsl/gsl_block.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_spmatrix.h>
#include <gsl/gsl_splinalg.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_permute_vector.h>
#include <gsl/gsl_permute_matrix.h>
#include <gsl/gsl_linalg.h>

namespace gsl
{

typedef gsl_complex complex;
typedef gsl_complex_float complex_float;

// =============================================================================
// Block
// =============================================================================

template<typename T>
struct generic_block
{
  size_t size;
  T *data;
};

template<typename T> struct block : generic_block<T> {};
template<> struct block<double> : gsl_block {};
template<> struct block<float> : gsl_block_float {};
template<> struct block<int> : gsl_block_int {};
template<> struct block<unsigned int> : gsl_block_uint {};
template<> struct block<long> : gsl_block_long {};
template<> struct block<unsigned long> : gsl_block_ulong {};
template<> struct block<short> : gsl_block_short {};
template<> struct block<unsigned short> : gsl_block_ushort {};
template<> struct block<char> : gsl_block_char {};
template<> struct block<unsigned char> : gsl_block_uchar {};
template<> struct block<complex> : gsl_block_complex {};
template<> struct block<complex_float> : gsl_block_complex_float {};

// =============================================================================
// Vector
// =============================================================================

template<typename T> struct generic_vector
{
  size_t size;
  size_t stride;
  T *data;
  block<T> *_block_;
  int owner;
};

template<typename T> struct vector_traits
{
  generic_vector<T> *id;
  vector_traits(size_t n) {id = 0;}
 ~vector_traits() {}
};

inline
gsl_vector_complex_view gsl_vector_complex_view_array(const gsl_complex *b, size_t n)
{ return ::gsl_vector_complex_view_array((double*)&b[0].dat[0], n); }
inline
gsl_vector_complex_float_view gsl_vector_complex_float_view_array(const gsl_complex_float *b, size_t n)
{ return ::gsl_vector_complex_float_view_array((float*)&b[0].dat[0], n); }

#define SPECIALIZE_VECTOR_TEMPLATE(TT,T,R) \
template<> struct vector_traits<TT> \
{ \
  typedef gsl_vector##T *gsl_type_ptr; \
  gsl_vector##T *id; \
  vector_traits() : id(0) {} \
  vector_traits(size_t n) {id = gsl_vector##T##_alloc(n);} \
  vector_traits(size_t n, const TT *data) { \
    id = gsl_vector##T##_alloc(n); \
    gsl_vector##T##_view vv = gsl_vector##T##_view_array(const_cast<TT*>(data), n); \
    gsl_vector##T##_memcpy(id, &vv.vector); } \
 ~vector_traits() {if (id != 0) gsl_vector##T##_free(id);} \
  vector_traits& operator=(const vector_traits& r) \
  { gsl_vector##T##_memcpy(id, r.id); return *this; } \
  TT get(const size_t i) {return gsl_vector##T##_get(id, i);} \
  void set(const size_t i, const TT R x) {gsl_vector##T##_set(id, i, x);} \
  TT *ptr(const size_t i) {return gsl_vector##T##_ptr(id, i);} \
  const TT *ptr(const size_t i) const {return gsl_vector##T##_const_ptr(id, i);} \
  const TT *const_ptr(const size_t i) const {return gsl_vector##T##_const_ptr(id, i);} \
  void set_all(const TT R x) {gsl_vector##T##_set_all(id, x);} \
  void set_zero() {gsl_vector##T##_set_zero(id);} \
  void set_basis(size_t i) {gsl_vector##T##_set_basis(id, i);} \
  int swap_elements(size_t i, size_t j) {return gsl_vector##T##_swap_elements(id, i, j);} \
  vector_traits& operator+=(const vector_traits& b) {gsl_vector##T##_add(id, b.id); return *this;} \
  vector_traits& operator-=(const vector_traits& b) {gsl_vector##T##_sub(id, b.id); return *this;} \
  vector_traits& operator*=(const vector_traits& b) {gsl_vector##T##_mul(id, b.id); return *this;} \
  vector_traits& operator/=(const vector_traits& b) {gsl_vector##T##_div(id, b.id); return *this;} \
  vector_traits& operator+=(const TT x) {gsl_vector##T##_add_constant(id, x); return *this;} \
  vector_traits& operator*=(const TT x) {gsl_vector##T##_scale(id, x); return *this;} \
}

SPECIALIZE_VECTOR_TEMPLATE(double,,);
SPECIALIZE_VECTOR_TEMPLATE(float,_float,);
SPECIALIZE_VECTOR_TEMPLATE(int,_int,);
SPECIALIZE_VECTOR_TEMPLATE(unsigned int,_uint,);
SPECIALIZE_VECTOR_TEMPLATE(long,_long,);
SPECIALIZE_VECTOR_TEMPLATE(unsigned long,_ulong,);
SPECIALIZE_VECTOR_TEMPLATE(short,_short,);
SPECIALIZE_VECTOR_TEMPLATE(unsigned short,_ushort,);
SPECIALIZE_VECTOR_TEMPLATE(char,_char,);
SPECIALIZE_VECTOR_TEMPLATE(unsigned char,_uchar,);
SPECIALIZE_VECTOR_TEMPLATE(complex,_complex,&);
SPECIALIZE_VECTOR_TEMPLATE(complex_float,_complex_float,&);

template<typename T>
struct vector : vector_traits<T>
{
  using B = vector_traits<T>;
  using gsl_type_ptr = typename B::gsl_type_ptr;
  vector() {}
  vector(size_t n) : vector_traits<T>(n) {}
  vector(size_t n, const T *v) : vector_traits<T>(n, v) {}
  vector(const vector<T>& r) : vector(r.size()) {*this = r;}
  size_t size() const {return vector_traits<T>::id->size;}
  vector<T>& operator=(const vector<T>& r)
  { *(vector_traits<T> *)this = r; return *this; }
  T& operator[](size_t i) {return static_cast<T&>(*B::ptr(i));}
  const T& operator[](size_t i) const {return static_cast<const T&>(*B::const_ptr(i));}
  bool operator==(std::nullptr_t n) const {return this->id == n;}
  bool operator!=(std::nullptr_t n) const {return !(*this == n);}
  bool is_null() const {return *this == nullptr;}
  struct reference
  {
    using id_type = typename B::gsl_type_ptr;
    id_type id;
    reference(id_type r) : id(r) {}
    size_t size() const
    { vector<T> v; v.id = id; auto sz = v.size(); v.id = 0; return sz; }
    reference& operator=(const vector<T>& r)
    { vector<T> v; v.id = id; v = r; v.id = 0; return *this; }
    T& operator[](size_t i)
    { vector<T> v; v.id = id; T& r = v[i]; v.id = 0; return r; }
    const T& operator[](size_t i) const
    { vector<T> v; v.id = id; const T& r = v[i]; v.id = 0; return r; }
    bool operator==(std::nullptr_t n) const
    { vector<T> v; v.id = id; bool res = (v == n); v.id = 0; return res; }
    bool operator!=(std::nullptr_t n) const {return !(*this == n);}
    bool is_null() const {return *this == nullptr;}
  };
};

// =============================================================================
// Matrix
// =============================================================================

template<typename T> struct generic_matrix
{
  size_t size1;
  size_t size2;
  size_t tda;
  double *data;
  block<T> *_block_;
  int owner;
};

template<typename T> struct matrix_traits
{
  generic_matrix<T> *id;
  matrix_traits(size_t n1, size_t n2) {id = 0;}
 ~matrix_traits() {}
};

inline
gsl_matrix_complex_view gsl_matrix_complex_view_array(const gsl_complex *b, size_t n1, size_t n2)
{ return ::gsl_matrix_complex_view_array((double*)&b[0].dat[0], n1, n2); }
inline
gsl_matrix_complex_float_view gsl_matrix_complex_float_view_array(const gsl_complex_float *b, size_t n1, size_t n2)
{ return ::gsl_matrix_complex_float_view_array((float*)&b[0].dat[0], n1, n2); }

#define SPECIALIZE_MATRIX_TEMPLATE(TT,T,R) \
template<> struct matrix_traits<TT> \
{ \
  typedef gsl_matrix##T *gsl_type_ptr; \
  gsl_matrix##T *id; \
  matrix_traits() : id(0) {} \
  matrix_traits(size_t n1, size_t n2) {id = gsl_matrix##T##_alloc(n1, n2);} \
  matrix_traits(size_t n1, size_t n2, const TT *data) { \
    id = gsl_matrix##T##_alloc(n1, n2); \
    gsl_matrix##T##_view mv = gsl_matrix##T##_view_array(const_cast<TT*>(data), n1, n2); \
    gsl_matrix##T##_memcpy(id, &mv.matrix); } \
 ~matrix_traits() {if (id != 0) gsl_matrix##T##_free(id);} \
  matrix_traits& operator=(const matrix_traits& r) \
  { gsl_matrix##T##_memcpy(id, r.id); return *this; } \
  TT get(const size_t i, const size_t j) {return gsl_matrix##T##_get(id, i, j);} \
  void set(const size_t i, const size_t j, const TT R x) {gsl_matrix##T##_set(id, i, j, x);} \
  TT *ptr(const size_t i, const size_t j) {return gsl_matrix##T##_ptr(id, i, j);} \
  const TT *const_ptr(const size_t i, const size_t j) {return gsl_matrix##T##_const_ptr(id, i, j);} \
  void set_all(const TT R x) {gsl_matrix##T##_set_all(id, x);} \
  void set_zero() {gsl_matrix##T##_set_zero(id);} \
  void set_identity() {gsl_matrix##T##_set_identity(id);} \
  int get_row(vector<TT>& v, size_t i) const {return gsl_matrix##T##_get_row(v.id, id, i);} \
  int get_col(vector<TT>& v, size_t j) const {return gsl_matrix##T##_get_col(v.id, id, j);} \
  int set_row(size_t i, const vector<TT>& v) {return gsl_matrix##T##_set_row(id, i, v.id);} \
  int set_col(size_t j, const vector<TT>& v) {return gsl_matrix##T##_set_col(id, j, v.id);} \
}

SPECIALIZE_MATRIX_TEMPLATE(double,,);
SPECIALIZE_MATRIX_TEMPLATE(float,_float,);
SPECIALIZE_MATRIX_TEMPLATE(int,_int,);
SPECIALIZE_MATRIX_TEMPLATE(unsigned int,_uint,);
SPECIALIZE_MATRIX_TEMPLATE(long,_long,);
SPECIALIZE_MATRIX_TEMPLATE(unsigned long,_ulong,);
SPECIALIZE_MATRIX_TEMPLATE(short,_short,);
SPECIALIZE_MATRIX_TEMPLATE(unsigned short,_ushort,);
SPECIALIZE_MATRIX_TEMPLATE(char,_char,);
SPECIALIZE_MATRIX_TEMPLATE(unsigned char,_uchar,);
SPECIALIZE_MATRIX_TEMPLATE(complex,_complex,&);
SPECIALIZE_MATRIX_TEMPLATE(complex_float,_complex_float,&);

template<typename T>
struct matrix : matrix_traits<T>
{
  using B = matrix_traits<T>;
  using gsl_type_ptr = typename B::gsl_type_ptr;
  matrix() {}
  matrix(size_t n1, size_t n2) : matrix_traits<T>(n1, n2) {}
  matrix(size_t n1, size_t n2, const T *a) : matrix_traits<T>(n1, n2, a) {}
  matrix(const matrix<T>& r) : matrix(r.size1(), r.size2()) {*this = r;}
  size_t size1() const {return matrix_traits<T>::id->size1;}
  size_t size2() const {return matrix_traits<T>::id->size2;}
  T& operator()(size_t i, size_t j) {return static_cast<T&>(*B::ptr(i, j));}
  matrix<T>& operator=(const matrix<T>& r)
  { *(matrix_traits<T> *)this = r; return *this; }
  const T& operator()(size_t i, size_t j) const
  { return static_cast<const T&>(*B::const_ptr(i, j)); }
  bool operator==(std::nullptr_t n) const {return this->id == n;}
  bool operator!=(std::nullptr_t n) const {return !(*this == n);}
  bool is_null() const {return *this == nullptr;}
};

#define IMPLEMENT_REAL_MATRIX_OPERATOR(T,W) \
inline vector<T> operator*(const matrix<T>& A, const vector<T>& x) { \
  vector<T> y(x.size()); \
  gsl_blas_##W##gemv(CblasNoTrans, 1, A.id, x.id, 0, y.id); \
  return y; }
#define IMPLEMENT_COMPLEX_MATRIX_OPERATOR(T,W) \
inline vector<T> operator*(const matrix<T>& A, const vector<T>& x) { \
  vector<T> y(x.size()); \
  gsl_blas_##W##gemv(CblasNoTrans, {1,0}, A.id, x.id, {0,0}, y.id); \
  return y; }

IMPLEMENT_REAL_MATRIX_OPERATOR(double,d)
IMPLEMENT_REAL_MATRIX_OPERATOR(float,s)
IMPLEMENT_COMPLEX_MATRIX_OPERATOR(complex,z)
IMPLEMENT_COMPLEX_MATRIX_OPERATOR(complex_float,c)

// =============================================================================
// Permutation
// =============================================================================

struct permutation
{
  gsl_permutation *id;
  permutation(size_t n) : id(gsl_permutation_alloc(n)) {gsl_permutation_init(id);}
 ~permutation() {gsl_permutation_free(id);}
  permutation& operator=(const permutation& r)
  { if (size() != r.size()) throw exception();
    gsl_permutation_memcpy(id, r.id); return *this; }

  size_t get(size_t i) const {return gsl_permutation_get(id, i);}
  size_t operator()(size_t i) const {return gsl_permutation_get(id, i);}

  size_t size() const {return gsl_permutation_size(id);}
  size_t *data() const {return gsl_permutation_data(id);}
  bool valid() const {return gsl_permutation_valid(id) == 0;}

  void reverse() {gsl_permutation_reverse(id);}
  int inverse(permutation& i) const {return gsl_permutation_inverse(i.id, id);}
  permutation inverse() const {permutation i(size()); inverse(i); return i;}
  int next() {return gsl_permutation_next(id);}
  int prev() {return gsl_permutation_prev(id);}
  permutation& operator++() {next(); return *this;}
  permutation& operator--() {prev(); return *this;}

  template<typename T>
  /*inline*/ int permute(vector<T>& v) const {throw exception_typename(); return -1;}
  template<typename T>
  /*inline*/ int permute_inverse(vector<T>& v) const {throw exception_typename(); return -1;}
  template<typename T>
  /*inline*/ int permute(matrix<T>& A) const {throw exception_typename(); return -1;}

  permutation operator*(const permutation& r) const
  {
    if (size() != r.size()) throw exception();
    permutation result(size());
    gsl_permutation_mul(result.id, id, r.id);
    return result;
  }

  struct exception : std::exception
  {
    virtual const char *what() const noexcept
    { return "gsl::permutation: Size mismatch."; }
  };
  struct exception_typename : std::exception
  {
    virtual const char *what() const noexcept
    { return "gsl::permutation: Unexpected template typename."; }
  };
};

#define SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(TT,T,M,S) \
template<> inline int permutation::permute##S(M<TT>& v) const \
{ return gsl_permute_##M##T##S(id, v.id); }

SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(double,,vector,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(float,_float,vector,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(int,_int,vector,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(unsigned int,_uint,vector,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(long,_long,vector,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(unsigned long,_ulong,vector,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(short,_short,vector,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(unsigned short,_ushort,vector,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(char,_char,vector,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(unsigned char,_uchar,vector,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(complex,_complex,vector,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(complex_float,_complex_float,vector,)

SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(double,,vector,_inverse)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(float,_float,vector,_inverse)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(int,_int,vector,_inverse)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(unsigned int,_uint,vector,_inverse)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(long,_long,vector,_inverse)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(unsigned long,_ulong,vector,_inverse)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(short,_short,vector,_inverse)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(unsigned short,_ushort,vector,_inverse)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(char,_char,vector,_inverse)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(unsigned char,_uchar,vector,_inverse)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(complex,_complex,vector,_inverse)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(complex_float,_complex_float,vector,_inverse)

SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(double,,matrix,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(float,_float,matrix,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(int,_int,matrix,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(unsigned int,_uint,matrix,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(long,_long,matrix,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(unsigned long,_ulong,matrix,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(short,_short,matrix,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(unsigned short,_ushort,matrix,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(char,_char,matrix,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(unsigned char,_uchar,matrix,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(complex,_complex,matrix,)
SPECIALIZE_PERMUTATION_METHOD_TEMPLATE(complex_float,_complex_float,matrix,)

// =============================================================================
// Linear Algebra
// =============================================================================

// -----------------------------------------------------------------------------
// 1. LU Decomposition
// -----------------------------------------------------------------------------
namespace LU {
inline int decomp(matrix<double>& A, permutation& p, int *signum)
{ return gsl_linalg_LU_decomp(A.id, p.id, signum); }
inline int decomp(matrix<complex>& A, permutation& p, int *signum)
{ return gsl_linalg_complex_LU_decomp(A.id, p.id, signum); }

inline int solve(const matrix<double>& LU, const permutation& p,
             const vector<double>& b, vector<double>& x)
{ return gsl_linalg_LU_solve(LU.id, p.id, b.id, x.id); }
inline int solve(const matrix<complex>& LU, const permutation& p,
             const vector<complex>& b, vector<complex>& x)
{ return gsl_linalg_complex_LU_solve(LU.id, p.id, b.id, x.id); }

inline int svx(const matrix<double>& LU, const permutation& p, vector<double>& x)
{ return gsl_linalg_LU_svx(LU.id, p.id, x.id); }
inline int svx(const matrix<complex>& LU, const permutation& p, vector<complex>& x)
{ return gsl_linalg_complex_LU_svx(LU.id, p.id, x.id); }

inline int refine(const matrix<double>& A, const matrix<double>& LU,
            const permutation& p,
            const vector<double>& b, vector<double>& x,
            vector<double>& work)
{ return gsl_linalg_LU_refine(A.id, LU.id, p.id, b.id, x.id, work.id); }
inline int refine(const matrix<complex>& A, const matrix<complex>& LU,
            const permutation& p,
            const vector<complex>& b, vector<complex>& x,
            vector<complex>& work)
{ return gsl_linalg_complex_LU_refine(A.id, LU.id, p.id, b.id, x.id, work.id); }

inline int invert(const matrix<double>& LU, const permutation& p, matrix<double>& inv)
{ return gsl_linalg_LU_invert(LU.id, p.id, inv.id); }
inline int invert(const matrix<complex>& LU, const permutation& p, matrix<complex>& inv)
{ return gsl_linalg_complex_LU_invert(LU.id, p.id, inv.id); }

inline double det(matrix<double>& LU, int signum)
{ return gsl_linalg_LU_det(LU.id, signum); }
inline complex det(matrix<complex>& LU, int signum)
{ return gsl_linalg_complex_LU_det(LU.id, signum); }

inline double lndet(matrix<double>& LU)
{ return gsl_linalg_LU_lndet(LU.id); }
inline double lndet(matrix<complex>& LU)
{ return gsl_linalg_complex_LU_lndet(LU.id); }

inline int sgndet(matrix<double>& LU, int signum)
{ return gsl_linalg_LU_sgndet(LU.id, signum) ; }

inline complex sgndet(matrix<complex>& LU, int signum)
{ return gsl_linalg_complex_LU_sgndet(LU.id, signum); }
} // namespace LU

// -----------------------------------------------------------------------------
// 2. QR Decomposition
// -----------------------------------------------------------------------------
namespace QR {
inline int decomp(matrix<double>& A, vector<double>& tau)
{ return gsl_linalg_QR_decomp(A.id, tau.id); }

inline int solve(const matrix<double>& QR, const vector<double>& tau,
    const vector<double>& b, vector<double>& x)
{ return gsl_linalg_QR_solve(QR.id, tau.id, b.id, x.id); }

inline int svx(const matrix<double>& QR, const vector<double>& tau, vector<double>& x)
{ return gsl_linalg_QR_svx(QR.id, tau.id, x.id); }

inline int lssolve(const matrix<double>& QR, const vector<double>& tau,
    const vector<double>& b, vector<double>& x, vector<double>& residual)
{ return gsl_linalg_QR_lssolve(QR.id, tau.id, b.id, x.id, residual.id); }

inline int QTvec(const matrix<double>& QR, const vector<double>& tau, vector<double>& v)
{ return gsl_linalg_QR_QTvec(QR.id, tau.id, v.id); }

inline int Qvec(const matrix<double>& QR, const vector<double>& tau, vector<double>& v)
{ return gsl_linalg_QR_Qvec(QR.id, tau.id, v.id); }

inline int QTmat(const matrix<double>& QR, const vector<double>& tau, matrix<double>& A)
{ return gsl_linalg_QR_QTmat(QR.id, tau.id, A.id); }

inline int Rsolve(const matrix<double>& QR, const vector<double>& b, vector<double>& x)
{ return gsl_linalg_QR_Rsolve(QR.id, b.id, x.id); }

inline int Rsvx(const matrix<double>& QR, vector<double>& x)
{ return gsl_linalg_QR_Rsvx(QR.id, x.id); }

inline int unpack(const matrix<double>& QR, const vector<double>& tau,
    matrix<double>& Q, matrix<double>& R)
{ return gsl_linalg_QR_unpack(QR.id, tau.id, Q.id, R.id); }

inline int QRsolve(matrix<double>& Q, matrix<double>& R, const vector<double>& b, vector<double>& x)
{ return gsl_linalg_QR_QRsolve(Q.id, R.id, b.id, x.id); }

inline int update(matrix<double>& Q, matrix<double>& R, vector<double>& w, const vector<double>& v)
{ return gsl_linalg_QR_update(Q.id, R.id, w.id, v.id); }

inline int R_solve(const matrix<double>& R, const vector<double>& b, vector<double>& x)
{ return gsl_linalg_R_solve(R.id, b.id, x.id); }

inline int R_svx(const matrix<double>& R, vector<double>& x)
{ return gsl_linalg_R_svx(R.id, x.id); }
} // namespace QR

// -----------------------------------------------------------------------------
// 3. QR Decomposition with Column Pivoting
// -----------------------------------------------------------------------------
namespace QRP {
inline int decomp(matrix<double>& A, vector<double>& tau, permutation& p,
    int *signum, vector<double>& norm)
{ return gsl_linalg_QRPT_decomp(A.id, tau.id, p.id, signum, norm.id); }

inline int decomp2(const matrix<double>& A, matrix<double>& q, matrix<double>& r,
    vector<double>& tau, permutation& p, int *signum, vector<double>& norm)
{ return gsl_linalg_QRPT_decomp2(A.id, q.id, r.id, tau.id, p.id, signum, norm.id); }

inline int solve(const matrix<double>& QR, const vector<double>& tau,
    const permutation& p, const vector<double>& b, vector<double>& x)
{ return gsl_linalg_QRPT_solve(QR.id, tau.id, p.id, b.id, x.id); }

inline int svx(const matrix<double>& QR, const vector<double>& tau,
    const permutation& p, vector<double>& x)
{ return gsl_linalg_QRPT_svx(QR.id, tau.id, p.id, x.id); }

inline int lssolve(const matrix<double>& QR, const vector<double>& tau,
    const permutation& p, const vector<double>& b,
    vector<double>& x, vector<double>& residual)
{ return gsl_linalg_QRPT_lssolve(QR.id, tau.id, p.id, b.id, x.id, residual.id); }

inline int lssolve2(const matrix<double>& QR, const vector<double>& tau,
    const permutation& p, const vector<double>& b, size_t rank,
    vector<double>& x, vector<double>& res)
{ return gsl_linalg_QRPT_lssolve2(QR.id, tau.id, p.id, b.id, rank, x.id, res.id); }

inline int QRsolve(const matrix<double>& Q, const matrix<double>& R,
    const permutation& p, const vector<double>& b, vector<double>& x)
{ return gsl_linalg_QRPT_QRsolve(Q.id, R.id, p.id, b.id, x.id); }

inline int update(matrix<double>& Q, matrix<double>& R, const permutation& p,
    vector<double>& w, const vector<double>& v)
{ return gsl_linalg_QRPT_update(Q.id, R.id, p.id, w.id, v.id); }

inline int Rsolve(const matrix<double>& QR, const permutation& p,
    const vector<double>& b, vector<double>& x)
{ return gsl_linalg_QRPT_Rsolve(QR.id, p.id, b.id, x.id); }

inline int Rsvx(const matrix<double>& QR, const permutation& p, vector<double>& x)
{ return gsl_linalg_QRPT_Rsvx(QR.id, p.id, x.id); }

inline size_t rank(const matrix<double>& QR, const double tol)
{ return gsl_linalg_QRPT_rank(QR.id, tol); }

inline int rcond(const matrix<double>& QR, double *rcond, vector<double>& work)
{ return gsl_linalg_QRPT_rcond(QR.id, rcond, work.id); }
} // namespace QRP

// -----------------------------------------------------------------------------
// 4. Complete Orthogonal Decomposition
// -----------------------------------------------------------------------------
namespace COD {
inline int decomp(matrix<double>& A, vector<double>& tau_Q, vector<double>& tau_Z,
    permutation& p, size_t *rank, vector<double>& work)
{ return gsl_linalg_COD_decomp(A.id, tau_Q.id, tau_Z.id, p.id, rank, work.id); }

inline int decomp(matrix<double>& A, vector<double>& tau_Q, vector<double>& tau_Z,
    permutation& p, double tol, size_t *rank, vector<double>& work)
{ return gsl_linalg_COD_decomp_e(A.id, tau_Q.id, tau_Z.id, p.id, tol, rank, work.id); }

inline int lssolve(const matrix<double>& QRZT, const vector<double>& tau_Q,
    const vector<double>& tau_Z, const permutation& p, const size_t rank,
    const vector<double>& b, vector<double>& x, vector<double>& residual)
{ return gsl_linalg_COD_lssolve(QRZT.id, tau_Q.id, tau_Z.id, p.id, rank, b.id,
                                x.id, residual.id); }

inline int lssolve2(const double lambda, const matrix<double>& QRZT,
    const vector<double>& tau_Q, const vector<double>& tau_Z,
    const permutation& p, const size_t rank, const vector<double>& b,
    vector<double>& x, vector<double>& residual, matrix<double>& S,
    vector<double>& work)
{ return gsl_linalg_COD_lssolve2(lambda, QRZT.id, tau_Q.id, tau_Z.id, p.id,
    rank, b.id, x.id, residual.id, S.id, work.id); }

inline int unpack(const matrix<double>& QRZT, const vector<double>& tau_Q,
           const vector<double>& tau_Z, const size_t rank, matrix<double>& Q,
           matrix<double>& R, matrix<double>& Z)
{ return gsl_linalg_COD_unpack(QRZT.id, tau_Q.id, tau_Z.id, rank, Q.id, R.id, Z.id); }

inline int matZ(const matrix<double>& QRZT, const vector<double>& tau_Z,
         const size_t rank, matrix<double>& A, vector<double>& work)
{ return gsl_linalg_COD_matZ(QRZT.id, tau_Z.id, rank, A.id, work.id); }
} // namespace COD

// -----------------------------------------------------------------------------
// 5. Singular Value Decomposition
// -----------------------------------------------------------------------------
namespace SV {
inline int decomp(matrix<double>& A, matrix<double>& V, vector<double>& S,
    vector<double>& work)
{ return gsl_linalg_SV_decomp(A.id, V.id, S.id, work.id); }

inline int decomp_mod(matrix<double>& A, matrix<double>& X, matrix<double>& V,
    vector<double>& S, vector<double>& work)
{ return gsl_linalg_SV_decomp_mod(A.id, X.id, V.id, S.id, work.id); }

inline int decomp_jacobi(matrix<double>& A, matrix<double>& V, vector<double>& S)
{ return gsl_linalg_SV_decomp_jacobi(A.id, V.id, S.id); }

inline int solve(const matrix<double>& U, const matrix<double>& V,
    const vector<double>& S, const vector<double>& b, vector<double>& x)
{ return gsl_linalg_SV_solve(U.id, V.id, S.id, b.id, x.id); }

inline int leverage(const matrix<double>& U, vector<double>& h)
{ return gsl_linalg_SV_leverage(U.id, h.id); }
} // namespace SV

// -----------------------------------------------------------------------------
// 6. Cholesky Decomposition
// -----------------------------------------------------------------------------
namespace cholesky {
inline int decomp1(matrix<double>& A)
{ return gsl_linalg_cholesky_decomp1(A.id); }

inline int decomp(matrix<complex>& A)
{ return gsl_linalg_complex_cholesky_decomp(A.id); }

inline int decomp(matrix<double>& A)
{ return gsl_linalg_cholesky_decomp(A.id); }

inline int solve(const matrix<double>& cholesky, const vector<double>& b,
    vector<double>& x)
{ return gsl_linalg_cholesky_solve(cholesky.id, b.id, x.id); }

inline int solve(const matrix<complex>& cholesky, const vector<complex>& b, vector<complex>& x)
{ return gsl_linalg_complex_cholesky_solve(cholesky.id, b.id, x.id); }

inline int svx(const matrix<double>& cholesky, vector<double>& x)
{ return gsl_linalg_cholesky_svx(cholesky.id, x.id); }

inline int svx(const matrix<complex>& cholesky, vector<complex>& x)
{ return gsl_linalg_complex_cholesky_svx(cholesky.id, x.id); }

inline int invert(matrix<double>& cholesky)
{ return gsl_linalg_cholesky_invert(cholesky.id); }

inline int invert(matrix<complex>& cholesky)
{ return gsl_linalg_complex_cholesky_invert(cholesky.id); }

inline int decomp2(matrix<double>& A, vector<double>& S)
{ return gsl_linalg_cholesky_decomp2(A.id, S.id); }

inline int solve2(const matrix<double>& cholesky, const vector<double>& S,
    const vector<double>& b, vector<double>& x)
{ return gsl_linalg_cholesky_solve2(cholesky.id, S.id, b.id, x.id); }

inline int svx2(const matrix<double>& cholesky, const vector<double>& S,
    vector<double>& x)
{ return gsl_linalg_cholesky_svx2(cholesky.id, S.id, x.id); }

inline int scale(const matrix<double>& A, vector<double>& S)
{ return gsl_linalg_cholesky_scale(A.id, S.id); }

inline int scale_apply(matrix<double>& A, const vector<double>& S)
{ return gsl_linalg_cholesky_scale_apply(A.id, S.id); }

inline int rcond(const matrix<double>& cholesky, double * rcond, vector<double>& work)
{ return gsl_linalg_cholesky_rcond(cholesky.id, rcond, work.id); }
} // namespace cholesky

// -----------------------------------------------------------------------------
// 7. Pivoted Cholesky Decomposition
// -----------------------------------------------------------------------------
namespace pcholesky {
inline int decomp(matrix<double>& A, permutation& p)
{ return gsl_linalg_pcholesky_decomp(A.id, p.id); }

inline int solve(const matrix<double>& LDLT, const permutation& p,
          const vector<double>& b, vector<double>& x)
{ return gsl_linalg_pcholesky_solve(LDLT.id, p.id, b.id, x.id); }

inline int svx(const matrix<double>& LDLT, const permutation& p, vector<double>& x)
{ return gsl_linalg_pcholesky_svx(LDLT.id, p.id, x.id); }

inline int decomp2(matrix<double>& A, permutation& p, vector<double>& S)
{ return gsl_linalg_pcholesky_decomp2(A.id, p.id, S.id); }

inline int solve2(const matrix<double>& LDLT, const permutation& p,
           const vector<double>& S, const vector<double>& b, vector<double>& x)
{ return gsl_linalg_pcholesky_solve2(LDLT.id, p.id, S.id, b.id, x.id); }

inline int svx2(const matrix<double>& LDLT, const permutation& p,
         const vector<double>& S, vector<double>& x)
{ return gsl_linalg_pcholesky_svx2(LDLT.id, p.id, S.id, x.id); }

inline int invert(const matrix<double>& LDLT, const permutation& p, matrix<double>& Ainv)
{ return gsl_linalg_pcholesky_invert(LDLT.id, p.id, Ainv.id); }

inline int rcond(const matrix<double>& LDLT, const permutation& p,
          double *rcond, vector<double>& work)
{ return gsl_linalg_pcholesky_rcond(LDLT.id, p.id, rcond, work.id); }
} // namespace pcholesky

// -----------------------------------------------------------------------------
// 8. Modified Cholesky Decomposition
// -----------------------------------------------------------------------------
namespace mcholesky {
inline int decomp(matrix<double>& A, permutation& p, vector<double>& E)
{ return gsl_linalg_mcholesky_decomp(A.id, p.id, E.id); }

inline int solve(const matrix<double>& LDLT, const permutation& p,
          const vector<double>& b, vector<double>& x)
{ return gsl_linalg_mcholesky_solve(LDLT.id, p.id, b.id, x.id); }

inline int svx(const matrix<double>& LDLT, const permutation& p, vector<double>& x)
{ return gsl_linalg_mcholesky_svx(LDLT.id, p.id, x.id); }

inline int rcond(const matrix<double>& LDLT, const permutation& p,
          double *rcond, vector<double>& work)
{ return gsl_linalg_mcholesky_rcond(LDLT.id, p.id, rcond, work.id); }
} // namespace mcholesky

// -----------------------------------------------------------------------------
// 9. Tridiagonal Decomposition of Real Symmetric Matrices
// -----------------------------------------------------------------------------
namespace symmtd {
inline int decomp(matrix<double>& A, vector<double>& tau)
{ return gsl_linalg_symmtd_decomp(A.id, tau.id); }

inline int unpack(const matrix<double>& A, const vector<double>& tau,
           matrix<double>& Q, vector<double>& diag, vector<double>& subdiag)
{ return gsl_linalg_symmtd_unpack(A.id, tau.id, Q.id, diag.id, subdiag.id); }

inline int unpack_T(const matrix<double>& A, vector<double>& diag, vector<double>& subdiag)
{ return gsl_linalg_symmtd_unpack_T(A.id, diag.id, subdiag.id); }
} // namespace symmtd

// -----------------------------------------------------------------------------
// 10. Tridiagonal Decomposition of Hermitian Matrices
// -----------------------------------------------------------------------------
namespace hermtd {
inline int decomp(matrix<complex>& A, vector<complex>& tau)
{ return gsl_linalg_hermtd_decomp(A.id, tau.id); }

inline int unpack(const matrix<complex>& A, const vector<complex>& tau,
    matrix<complex>& U, vector<double>& diag, vector<double>& subdiag)
{ return gsl_linalg_hermtd_unpack(A.id, tau.id, U.id, diag.id, subdiag.id); }

inline int unpack_T(const matrix<complex>& A, vector<double>& diag,
    vector<double>& subdiag)
{ return gsl_linalg_hermtd_unpack_T(A.id, diag.id, subdiag.id); }
} // namespace hermtd

// -----------------------------------------------------------------------------
// 11. Hessenberg Decomposition of Real Matrices
// -----------------------------------------------------------------------------
namespace hessenberg {
inline int decomp(matrix<double>& A, vector<double>& tau)
{ return gsl_linalg_hessenberg_decomp(A.id, tau.id); }

inline int unpack(matrix<double>& H, vector<double>& tau, matrix<double>& U)
{ return gsl_linalg_hessenberg_unpack(H.id, tau.id, U.id); }

inline int unpack_accum(matrix<double>& H, vector<double>& tau, matrix<double>& V)
{ return gsl_linalg_hessenberg_unpack_accum(H.id, tau.id, V.id); }

inline int set_zero(matrix<double>& H)
{ return gsl_linalg_hessenberg_set_zero(H.id); }
} // namespace hessenberg

// -----------------------------------------------------------------------------
// 12. Hessenberg-Triangular Decomposition of Real Matrices
// -----------------------------------------------------------------------------
namespace hesstri {
inline int decomp(matrix<double>& A, matrix<double>& B, matrix<double>& U,
           matrix<double>& V, vector<double>& work)
{ return gsl_linalg_hesstri_decomp(A.id, B.id, U.id, V.id, work.id); }
} // namespace hesstri

// -----------------------------------------------------------------------------
// 13. Bidiagonalization
// -----------------------------------------------------------------------------
namespace bidiag {
inline int decomp(matrix<double>& A, vector<double>& tau_U, vector<double>& tau_V)
{ return gsl_linalg_bidiag_decomp(A.id, tau_U.id, tau_V.id); }

inline int unpack(const matrix<double>& A, const vector<double>& tau_U,
           matrix<double>& U, const vector<double>& tau_V, matrix<double>& V,
           vector<double>& diag, vector<double>& superdiag)
{ return gsl_linalg_bidiag_unpack(A.id, tau_U.id, U.id, tau_V.id, V.id, diag.id,
    superdiag.id); }

inline int unpack2(matrix<double>& A, vector<double>& tau_U, vector<double>& tau_V,
            matrix<double>& V)
{ return gsl_linalg_bidiag_unpack2(A.id, tau_U.id, tau_V.id, V.id); }

inline int unpack_B(const matrix<double>& A, vector<double>& diag, vector<double>& superdiag)
{ return gsl_linalg_bidiag_unpack_B(A.id, diag.id, superdiag.id); }
} // namespace bidiag

// -----------------------------------------------------------------------------
// 14. Givens Rotations
// -----------------------------------------------------------------------------
namespace givens {
inline void gm(const double a, const double b, double *c, double *s)
{ gsl_linalg_givens(a, b, c, s); }

inline void gv(vector<double>& v, size_t i, size_t j, double c, double s)
{ gsl_linalg_givens_gv(v.id, i, j, c, s); }
} // namespace givens

namespace householder {
// -----------------------------------------------------------------------------
// 15. Householder Transformations
// -----------------------------------------------------------------------------
inline double transform(vector<double>& w)
{ return gsl_linalg_householder_transform(w.id); }
inline complex transform(vector<complex>& w)
{ return gsl_linalg_complex_householder_transform(w.id); }

inline int hm(double tau, const vector<double>& v, matrix<double>& A)
{ return gsl_linalg_householder_hm(tau, v.id, A.id); }
inline int hm(complex tau, const vector<complex>& v, matrix<complex>& A)
{ return gsl_linalg_complex_householder_hm(tau, v.id, A.id); }

inline int mh(double tau, const vector<double>& v, matrix<double>& A)
{ return gsl_linalg_householder_mh(tau, v.id, A.id); }
inline int mh(complex tau, const vector<complex>& v, matrix<complex>& A)
{ return gsl_linalg_complex_householder_mh(tau, v.id, A.id); }

inline int hv(double tau, const vector<double>& v, vector<double>& w)
{ return gsl_linalg_householder_hv(tau, v.id, w.id); }
inline int hv(complex tau, const vector<complex>& v, vector<complex>& w)
{ return gsl_linalg_complex_householder_hv(tau, v.id, w.id); }

// -----------------------------------------------------------------------------
// 16. Householder Solver
// -----------------------------------------------------------------------------
inline int solve(matrix<double>& A, const vector<double>& b, vector<double>& x)
{ return gsl_linalg_HH_solve(A.id, b.id, x.id); }

inline int svx(matrix<double>& A, vector<double>& x)
{ return gsl_linalg_HH_svx(A.id, x.id); }
} // namespace householder

// -----------------------------------------------------------------------------
// 17. Tridiagonal Systems
// -----------------------------------------------------------------------------
namespace tridiag {
inline int solve(const vector<double>& diag, const vector<double>& e,
          const vector<double>& f, const vector<double>& b, vector<double>& x)
{ return gsl_linalg_solve_tridiag(diag.id, e.id, f.id, b.id, x.id); }

inline int solve_symm(const vector<double>& diag, const vector<double>& e,
          const vector<double>& b, vector<double>& x)
{ return gsl_linalg_solve_symm_tridiag(diag.id, e.id, b.id, x.id); }

inline int solve_cyc(const vector<double>& diag, const vector<double>& e,
          const vector<double>& f, const vector<double>& b, vector<double>& x)
{ return gsl_linalg_solve_cyc_tridiag(diag.id, e.id, f.id, b.id, x.id); }

inline int solve_symm_cyc(const vector<double>& diag, const vector<double>& e,
          const vector<double>& b, vector<double>& x)
{ return gsl_linalg_solve_symm_cyc_tridiag(diag.id, e.id, b.id, x.id); }
} // namespace tridiag

// -----------------------------------------------------------------------------
// 18. Triangular Systems
// -----------------------------------------------------------------------------
namespace tri {
inline int upper_invert(matrix<double>& T)
{ return gsl_linalg_tri_upper_invert(T.id); }

inline int lower_invert(matrix<double>& T)
{ return gsl_linalg_tri_lower_invert(T.id); }

inline int upper_unit_invert(matrix<double>& T)
{ return gsl_linalg_tri_upper_unit_invert(T.id); }

inline int lower_unit_invert(matrix<double>& T)
{ return gsl_linalg_tri_lower_unit_invert(T.id); }

inline int upper_rcond(const matrix<double>& T, double *rcond, vector<double>& work)
{ return gsl_linalg_tri_upper_rcond(T.id, rcond, work.id); }

inline int lower_rcond(const matrix<double>& T, double *rcond, vector<double>& work)
{ return gsl_linalg_tri_lower_rcond(T.id, rcond, work.id); }
} // namespace tri

// -----------------------------------------------------------------------------
// Matrix Balancing
// -----------------------------------------------------------------------------
inline int balance_matrix(matrix<double>& A, vector<double>& D)
{ return gsl_linalg_balance_matrix(A.id, D.id); }

// =============================================================================
// Sparse Linear Algebra
// =============================================================================

namespace sp {

// -----------------------------------------------------------------------------
// Sparse Matrix
// -----------------------------------------------------------------------------

template<typename T> struct generic_matrix
{
  size_t size1;
  size_t size2;
  size_t *i;
  T *data;
  size_t *p;
  size_t nzmax;
  size_t nz;
  gsl_spmatrix_tree *tree_data;
  void *work;
  size_t sptype;
};

template<typename T> struct matrix_traits
{
  generic_matrix<T> *id;
  matrix_traits(size_t n1, size_t n2=1) {id = 0;}
 ~matrix_traits() {}
};

#define SPECIALIZE_SPMATRIX_TEMPLATE(TT,T,R) \
template<> struct matrix_traits<TT> \
{ \
  gsl_spmatrix##T *id; \
  matrix_traits() : destroy(false) {id = 0;} \
  matrix_traits(size_t n1, size_t n2=1) : \
    destroy(true) \
  { id = gsl_spmatrix##T##_alloc(n1, n2); } \
  matrix_traits(size_t sptype, size_t nzmax, size_t n1, size_t n2=1) : \
    destroy(true) \
  { id = gsl_spmatrix##T##_alloc_nzmax(n1, n2, nzmax, sptype); } \
 ~matrix_traits() {if (destroy) gsl_spmatrix##T##_free(id);} \
  TT get(const size_t i, const size_t j) \
  { return gsl_spmatrix##T##_get(id, i, j); } \
  void set(const size_t i, const size_t j, const TT R x) \
  { gsl_spmatrix##T##_set(id, i, j, x); } \
  TT *ptr(const size_t i, const size_t j) \
  { return gsl_spmatrix##T##_ptr(id, i, j); } \
  const TT *const_ptr(const size_t i, const size_t j) const \
  { matrix_traits<TT> *t = const_cast<matrix_traits<TT>*>(this); \
    return const_cast<const TT *>(t->ptr(i, j)); } \
  void set_zero() {gsl_spmatrix##T##_set_zero(id);} \
  size_t nnz() const {return gsl_spmatrix##T##_nnz(id);} \
  matrix_traits<TT>& operator=(const matrix_traits<TT>& r) \
  { if (r.destroy) throw matrix_exception(); \
    id = r.id; destroy = true; return *this; } \
  matrix_traits<TT> ccs() \
  { matrix_traits<TT> r; r.destroy = false; \
    r.id = gsl_spmatrix##T##_ccs(id); return r; } \
  matrix_traits<TT> crs() \
  { matrix_traits<TT> r; r.destroy = false; \
    r.id = gsl_spmatrix##T##_crs(id); return r; } \
  struct matrix_exception : std::exception { \
    virtual const char * \
    what() const noexcept {return "gsl::sp::matrix: Bad use of copy operator.";} \
  }; \
private: \
  bool destroy; \
}

SPECIALIZE_SPMATRIX_TEMPLATE(double,,);
#if 0
SPECIALIZE_SPMATRIX_TEMPLATE(float,_float,);
SPECIALIZE_SPMATRIX_TEMPLATE(int,_int,);
SPECIALIZE_SPMATRIX_TEMPLATE(unsigned int,_uint,);
SPECIALIZE_SPMATRIX_TEMPLATE(long,_long,);
SPECIALIZE_SPMATRIX_TEMPLATE(unsigned long,_ulong,);
SPECIALIZE_SPMATRIX_TEMPLATE(short,_short,);
SPECIALIZE_SPMATRIX_TEMPLATE(unsigned short,_ushort,);
SPECIALIZE_SPMATRIX_TEMPLATE(char,_char,);
SPECIALIZE_SPMATRIX_TEMPLATE(unsigned char,_uchar,);
SPECIALIZE_SPMATRIX_TEMPLATE(complex,_complex,&);
SPECIALIZE_SPMATRIX_TEMPLATE(complex_float,_complex_float,&);
#endif // 0

template<typename T>
struct matrix : matrix_traits<T>
{
  using B = matrix_traits<T>;

  struct tmp
  {
    matrix<T> *_this;
    T *ptr;
    size_t i, j;
    tmp(matrix<T> *self, T *p, size_t I, size_t J) :
      _this(self), ptr(p), i(I), j(J) {}
    operator T() {return ptr == 0 ? T(0) : *ptr;}
    operator const T() const {return ptr == 0 ? T(0) : *ptr;}
    T& operator=(const T& r)
    { if (ptr == 0) _this->set(i,j,r); return *_this->ptr(i,j); }
  };

  matrix() {}
  matrix(size_t n1, size_t n2=1) : matrix_traits<T>(n1, n2) {}
  matrix(size_t sptype, size_t nzmax, size_t n1, size_t n2=1) :
    matrix_traits<T>(sptype, nzmax, n1, n2) {}
  matrix(const matrix_traits<T>& r) {*(matrix_traits<T> *)this = r;}
  tmp operator()(size_t i, size_t j)
  { return tmp(this, B::ptr(i, j), i, j); }
  const tmp operator()(size_t i, size_t j) const
  { auto t = const_cast<matrix<T>*>(this); return tmp(t, t->ptr(i, j), i, j); }
  matrix<T>& operator=(const matrix_traits<T>& r)
  { *(matrix_traits<T> *)this = r; return *this; }
};

// -----------------------------------------------------------------------------
// Iterative solver - GMRES
// -----------------------------------------------------------------------------

template<typename T>
struct itersolve
{
  using itersolve_type = gsl_splinalg_itersolve_type;

  gsl_splinalg_itersolve *id;

  itersolve(size_t n, size_t m = 0, const itersolve_type *t = gsl_splinalg_itersolve_gmres) :
    id(gsl_splinalg_itersolve_alloc(t, n, m)) {}
 ~itersolve() {gsl_splinalg_itersolve_free(id);}
  const char *name() const {return gsl_splinalg_itersolve_name(id);}

  int iterate(const matrix<T>& A, const gsl::vector<T>& b, double tol, gsl::vector<T>& x)
  { return gsl_splinalg_itersolve_iterate(A.id, b.id, tol, x.id, id); }

  double normr() const {return gsl_splinalg_itersolve_normr(id);}

  int solve(matrix<T>& A, const gsl::vector<T>& b, gsl::vector<T>& u,
            double tol, std::function<bool(vector<T>&, int)> fcallback)
  {
    if (tol < 0.) tol = 1.e-8;
    int status, iter = 1;
    do {
      status = iterate(A, b, tol, u);
      if (fcallback != 0 && !fcallback(u, iter++)) break;
    } while (status == GSL_CONTINUE);
    return status;
  }

  int solve(matrix<T>& A, const gsl::vector<T>& b, gsl::vector<T>& u,
            double tol = -1., int max_iter = -1)
  {
    if (max_iter == 0) return GSL_SUCCESS;
    if (max_iter < 1) max_iter = 100;
    return solve(A, b, u, tol, [max_iter](gsl::vector<T>& u_arg, int iter_arg) {
      return iter_arg < max_iter;
    });
  }
};

} // namespace sp

} // namespace gsl

#endif // GSL_CPP_HPP_INCLUDED
