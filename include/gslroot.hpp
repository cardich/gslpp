// This file is part of gsl++
// (c) Apostol Faliagas, 2018

#ifndef GSLROOT_HPP_INCLUDED
#define GSLROOT_HPP_INCLUDED

#include <functional>
#include <gsl/gsl_errno.h>
// #include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_multiroots.h>
#include "gsl.hpp"

namespace gsl {
namespace root {

class fsolver
{
public:
  enum { BISECTION = 0, BRENT, FALSEPOS };

  gsl_root_fsolver *id;
  std::function<double(double)> lambda;

  explicit fsolver(int type)
  {
    static const gsl_root_fsolver_type *fsolver_types[] = {
      gsl_root_fsolver_bisection,
      gsl_root_fsolver_brent,
      gsl_root_fsolver_falsepos
    };
    id = gsl_root_fsolver_alloc(fsolver_types[type]);
  }
  ~fsolver() {gsl_root_fsolver_free(id);}
  const char *name() const {return gsl_root_fsolver_name(id);}
  int set(double x_lower, double x_upper, std::function<double(double)> f)
  {
    lambda = f;
    _data = {&fsolver::_f, this};
    return gsl_root_fsolver_set(id, &_data, x_lower, x_upper);
  }
  int iterate() {return gsl_root_fsolver_iterate(id);}
  double root() const {return gsl_root_fsolver_root(id);}
  double x_lower() const {return gsl_root_fsolver_x_lower(id);}
  double x_upper() const {return gsl_root_fsolver_x_upper(id);}
  static int test_interval(double x_lower, double x_upper, double epsa, double epsr)
  { return gsl_root_test_interval(x_lower, x_upper, epsa, epsr); }
  static int test_delta(double x1, double x0, double epsabs, double epsrel)
  { return gsl_root_test_delta(x1, x0, epsabs, epsrel); }
  static int test_residual(double f, double epsabs)
  { return gsl_root_test_residual(f, epsabs); }
  int solve(int max_iter, std::function<bool(int&,int)> ctrl = 0)
  {
    if (max_iter < 0 && ctrl == 0) return -1;
    int iter = 0, status;
    do {
      ++iter;
      status = iterate();
      if (ctrl != 0 && !ctrl(status, iter)) break;
    } while (max_iter < 0 || iter < max_iter);
    return status;
  }

protected:
  gsl_function _data;
  static double _f(double x, void *p)
  { return ((fsolver *) p)->lambda(x); }
};

class fdfsolver
{
public:
  enum { NEWTON = 0, SECANT, STEFFENSON };
  gsl_root_fdfsolver *id;
  std::function<void(double, double*, double*)> lambda;

  explicit fdfsolver(int type)
  {
    static const gsl_root_fdfsolver_type *fdfsolver_types[] = {
      gsl_root_fdfsolver_newton,
      gsl_root_fdfsolver_secant,
      gsl_root_fdfsolver_steffenson
    };
    id = gsl_root_fdfsolver_alloc(fdfsolver_types[type]);
  }
  ~fdfsolver() {gsl_root_fdfsolver_free(id);}
  const char *name() const {return gsl_root_fdfsolver_name(id);}
  int set(double r, std::function<void(double, double*, double*)> fdf)
  {
    lambda = fdf;
    _data = {&_f, &_df, &_fdf, this};
    return gsl_root_fdfsolver_set(id, &_data, r);
  }
  int iterate() {return gsl_root_fdfsolver_iterate(id);}
  double root() const {return gsl_root_fdfsolver_root(id);}
  static int test_delta(double x1, double x0, double epsabs, double epsrel)
  { return gsl_root_test_delta(x1, x0, epsabs, epsrel); }
  static int test_residual(double f, double epsabs)
  { return gsl_root_test_residual(f, epsabs); }
  int solve(int max_iter, std::function<bool(int&,int,double)> ctrl = 0)
  {
    if (max_iter < 0 && ctrl == 0) return -1;
    int iter = 0, status;
    do {
      ++iter;
      double prevsol = root();
      status = iterate();
      if (ctrl != 0 && !ctrl(status, iter, prevsol)) break;
    } while (max_iter < 0 || iter < max_iter);
    return status;
  }

protected:
  gsl_function_fdf _data;
  static double _f(double x, void *p)
  {
    double y, dy;
    _fdf(x, p, &y, &dy);
    return y;
  }
  static double _df(double x, void *p)
  {
    double y, dy;
    _fdf(x, p, &y, &dy);
    return dy;
  }
  static void _fdf(double x, void *p, double *y, double *dy)
  {
    return ((fdfsolver *) p)->lambda(x, y, dy);
  }
};

} // namespace root

namespace multiroot {

class fsolver
{
public:
  enum { HYBRIDS = 0, HYBRID, DNEWTON, BROYDEN };

  gsl_multiroot_fsolver *id;
  std::function<int(const vector<double>&, vector<double>&)> lambda;

  explicit fsolver(int type, int n)
  {
    static const gsl_multiroot_fsolver_type *fsolver_types[] = {
      gsl_multiroot_fsolver_hybrids,
      gsl_multiroot_fsolver_hybrid,
      gsl_multiroot_fsolver_dnewton,
      gsl_multiroot_fsolver_broyden
    };
    id = gsl_multiroot_fsolver_alloc(fsolver_types[type], n);
  }
  ~fsolver() {gsl_multiroot_fsolver_free(id);}
  const char *name() const {return gsl_multiroot_fsolver_name(id);}
  int set(const vector<double>& r,
          std::function<int(const vector<double>&, vector<double>&)> f)
  {
    lambda = f;
    _data = {_f, r.size(), (void *)this};
    return gsl_multiroot_fsolver_set(id, &_data, r.id);
  }
  int iterate() {return gsl_multiroot_fsolver_iterate(id);}
  vector<double>::reference root() const
  { return vector<double>::reference(gsl_multiroot_fsolver_root(id)); }
  vector<double>::reference f() const
  { return vector<double>::reference(gsl_multiroot_fsolver_f(id)); }
  vector<double>::reference dx() const
  { return vector<double>::reference(gsl_multiroot_fsolver_dx(id)); }
  static int test_delta(const vector<double>::reference& dx,
      const vector<double>::reference& x, double epsabs, double epsrel)
  { return gsl_multiroot_test_delta(dx.id, x.id, epsabs, epsrel); }
  static int test_residual(const vector<double>::reference& f, double epsabs)
  { return gsl_multiroot_test_residual(f.id, epsabs); }
  int solve(int max_iter, std::function<bool(int&,int)> ctrl = 0)
  {
    if (max_iter < 0 && ctrl == 0) return -1;
    int iter = 0, status;
    do {
      iter++;
      status = iterate();
      if (ctrl != 0)
      { if (!ctrl(status, iter)) break; }
      else if ((status = fsolver::test_residual(f(), 1e-7)) != GSL_CONTINUE)
      { break; }
    } while (max_iter < 0 || iter < max_iter);
    return status;
  }

protected:
  gsl_multiroot_function _data;
  static int _f(const gsl_vector *x, void *p, gsl_vector *y)
  {
    vector<double> v, w;
    v.id = const_cast<gsl_vector *>(x); w.id = y;
    int result = ((fsolver *) p)->lambda(v, w);
    v.id = 0; w.id = 0; // escape destructor!
    return result;
  }
};

class fdfsolver
{
public:
  enum { HYBRIDSJ = 0, HYBRIDJ, NEWTON, GNEWTON };
  gsl_multiroot_fdfsolver *id;
  std::function<int(const vector<double>&,vector<double>&,matrix<double>&)> lambda;

  explicit fdfsolver(int type, int n)
  {
    static const gsl_multiroot_fdfsolver_type *fdfsolver_types[] = {
      gsl_multiroot_fdfsolver_hybridsj,
      gsl_multiroot_fdfsolver_hybridj,
      gsl_multiroot_fdfsolver_newton,
      gsl_multiroot_fdfsolver_gnewton
    };
    id = gsl_multiroot_fdfsolver_alloc(fdfsolver_types[type], n);
  }
  ~fdfsolver() {gsl_multiroot_fdfsolver_free(id);}
  const char *name() const {return gsl_multiroot_fdfsolver_name(id);}
  int set(const vector<double>& r,
    std::function<int(const vector<double>&,vector<double>&,matrix<double>&)> f)
  {
    lambda = f;
    _data = {&_f, &_df, &_fdf, r.size(), this};
    return gsl_multiroot_fdfsolver_set(id, &_data, r.id);
  }
  int iterate() {return gsl_multiroot_fdfsolver_iterate(id);}
  vector<double>::reference root() const
  { return vector<double>::reference(gsl_multiroot_fdfsolver_root(id)); }
  vector<double>::reference f() const
  { return vector<double>::reference(gsl_multiroot_fdfsolver_f(id)); }
  vector<double>::reference dx() const
  { return vector<double>::reference(gsl_multiroot_fdfsolver_dx(id)); }
  static int test_delta(const vector<double>::reference& dx,
      const vector<double>::reference& x, double epsabs, double epsrel)
  { return gsl_multiroot_test_delta(dx.id, x.id, epsabs, epsrel); }
  static int test_residual(const vector<double>::reference& f, double epsabs)
  { return gsl_multiroot_test_residual(f.id, epsabs); }
  int solve(int max_iter, std::function<bool(int&,int)> ctrl = 0)
  {
    if (max_iter < 0 && ctrl == 0) return -1;
    int iter = 0, status;
    do {
      iter++;
      status = iterate();
      if (ctrl != 0)
      { if (!ctrl(status, iter)) break; }
      else if ((status = fsolver::test_residual(f(), 1e-7)) != GSL_CONTINUE)
      { break; }
    } while (max_iter < 0 || iter < max_iter);
    return status;
  }

protected:
  gsl_multiroot_function_fdf _data;
  static int _f(const gsl_vector *x, void *p, gsl_vector *y)
  { return _fdf(x, p, y, 0); }
  static int _df(const gsl_vector *x, void *p, gsl_matrix *J)
  { return _fdf(x, p, 0, J); }
  static int _fdf(const gsl_vector *x, void *p, gsl_vector *y, gsl_matrix *J)
  {
    vector<double> u, w;
    matrix<double> A;
    u.id = const_cast<gsl_vector *>(x); w.id = y; A.id = J;
    int result = ((fdfsolver *) p)->lambda(u, w, A);
    u.id = 0; w.id = 0; A.id = 0;
    return result;
  }
};

} // namespace multiroot
} // namespace gsl

#endif // GSLROOT_HPP_INCLUDED
