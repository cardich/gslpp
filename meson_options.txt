## meson_options.txt - meson build options file for gsl++.
## Copyright (C) 2018 apostol <apostol.faliagas@gmail.com>
## Rev. 0 (original) -- Sun 14 Oct 2018 08:42:00 EEST

option('compile-examples', type : 'boolean', value : true, description: 'Compile examples')
option('extra-diags', type : 'boolean', value : false, description: 'Enable additional diagnostics')
## Change this option to true to select a private, non-system installation
option('no-sysinst', type : 'boolean', value : false, description: 'Select private installation')
## Change this option when the package name changes
option('pkg-name', type : 'string', value : 'gsl++', description: 'Set package name')
## Change this option when the library's API changes
option('lib-version', type : 'string', value : '0.0.0', description: 'Set library version')
option('soversion', type : 'string', value : 'default', description: 'Set soversion')
