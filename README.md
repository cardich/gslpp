## Introduction

**Gsl++** is a C++ wrapper for GSL.

Gsl++ was initially created with the purpose to
serve the **fidlab** code generator for linear
and non-linear Partial Differential Equations.
As a consequence, it is not complete, but even so
one can use gsl++ as a stand-alone library.

Currently, gsl++ supports the following classes:

- `vector`
- `matrix`
- Linear Algebra methods
- `root::fsolver`, `multiroot::fsolver`
- `root::fdfsolver`, `multiroot::fdfsolver`
- ODE integration

and their dependencies.

Gsl++ is mostly inline and template implemented. It has
no binary libraries. It follows the original GSL naming
conventions, as far as possible, replacing underscore
separated name fragments with C++ namespaces. For
example, the GSL function

```c
gsl_matrix_double_get_row()
```

in gsl++ becomes

```cpp
gsl::matrix<double>::get_row().
```

Another example is

```c
gsl_multiroot_fdfsolver_iterate(),
```

which in gsl++ is

```cpp
gsl::multiroot::fdfsolver::iterate().
```

More extensive documentation and tutorials are under way.

## Installation

Gsl++ can be installed either from source or from a
pre-built installer (deb file).

### A. Installation from source

To install from source:

Install the `meson` build system (including `ninja`), if it is
not already installed, with a software manager, eg. `Synaptic`,
or from the command line:

```bash
sudo apt install meson
```

This step will also install `ninja`.

If you are to install in a *non-system* directory, edit
the file `meson_build.txt` in the root directory of
your source so that the value of the option `no-sysinst` is
`true`. It is important that this line looks like this:

```py
option('no-sysinst', type : 'boolean', value : false,
description: 'Select private installation')
```

For a *system* installation leave `no-sysinst` to `false`.
You may want to change other options, as for example,
`compile-examples` in `meson_build.txt`.

In the root directory of your source create a subdirectory
`nsbuild` and make it current:

```bash
mkdir nsbuild
cd nsbuild
```

Run `meson` to configure your build:

```bash
meson --prefix ~/my/nonsystem/installation/dir ..
```

Run `ninja` to build the package:

```bash
ninja
```

Run `ninja` to install it:

```bash
ninja install
```

Gsl++ will be installed in `~/my/nonsystem/installation/dir`.

### B. Installation with a package manager

Use your software manager, or the command line

```bash
sudo apt install gsl++
```

after having added the library's repository. A PPA repository
is available by the 
[author](https://launchpad.net/~faliagas/+archive/ubuntu/fidlab).

### C. Use from Docker container

A Docker container containing gsl++ along with PETSc, fidlab
etc is available at
[dockerhub](https://hub.docker.com/repository/docker/apostol/fidlab).


## Usage

See supplied examples.
