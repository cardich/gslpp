#include <cstdio>
#include "gslroot.hpp"

using namespace gsl::root;

int main()
{
  int max_iter = 100;
  double r_expected = sqrt(5.0);
  double x_lo = 0.0, x_hi = 5.0;
  const double a = 1.0, b = 0.0, c = -5.0;

  fsolver s(fsolver::BRENT);
  #if 0
  s.set(x_lo, x_hi, [a,b,c](double x, void *p, double *y, double *dy) {
    *y = (a*x + b)*x + c;
    *dy = 2.0*a*x + b;
  });
  #endif // 0
  s.set(x_lo, x_hi, [a,b,c](double x) {return (a*x + b)*x + c;});

  printf("using %s method\n", s.name());
  printf("%5s [%9s, %9s] %9s %10s %9s\n",
         "iter", "lower", "upper", "root", "err", "err(est)");

  int status = s.solve(2, [&s, r_expected, max_iter](int& status, int iter) -> bool {
    double r    = s.root();
    double x_lo = s.x_lower();
    double x_hi = s.x_upper();
    status  = fsolver::test_interval(x_lo, x_hi, 0, 0.001);
    if (status == GSL_SUCCESS) printf("Converged:\n");
    printf("%5d [%.7f, %.7f] %.7f %+.7f %.7f\n",
           iter, x_lo, x_hi, r, r - r_expected, x_hi - x_lo);
    return status != GSL_SUCCESS/* && iter < max_iter*/;
  });

  return status;
}
