#include <iostream>
#include "gslevol.hpp"

struct MatElemAccessor {
  const size_t dim;
  inline size_t operator()(size_t i, size_t j) const noexcept {
    return i*dim+j;
  }
};

using namespace gsl::odeiv2;

int main()
{
  const double mu = 10;
  gsl::odeiv2::system sys{2, [mu](double t, const double y[], double f[]) {
      f[0] = y[1];
      f[1] = -y[0] - mu*y[1]*(y[0]*y[0] - 1);
      return GSL_SUCCESS;
    }, [mu, &sys](double t, const double y[], double *dfdy, double dfdt[]) {
      MatElemAccessor e{sys.dimension()};
      dfdy[e(0, 0)] = 0.0;
      dfdy[e(0, 1)] = 1.0;
      dfdy[e(1, 0)] = -2.0*mu*y[0]*y[1] - 1.0;
      dfdy[e(1, 1)] = -mu*(y[0]*y[0] - 1.0);
      dfdt[0] = 0.0;
      dfdt[1] = 0.0;
      return GSL_SUCCESS;
    }
  };
  driver d(sys, RK4IMP, 1e-6, 1e-6, 0.0);

  double t = 0.0, t1 = 100.0;
  double y[2]{1.0, 0.0};

  for (int i = 1; i <= 100; i++)
  {
    double ti = i * t1 / 100.0;
    int status = d.apply(t, ti, y);
    if (status != GSL_SUCCESS)
    {
      std::cout << "error, return value " << status << "\n";
      break;
    }
    std::cout << t << " " << y[0] << " " << y[1] << "\n";
  }

  return 0;
}
