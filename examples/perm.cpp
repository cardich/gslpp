#include <iostream>
#include "gsl.hpp"

int main()
{
  const size_t N = 10;
  gsl::permutation p(N), q(N);

  std::cout << "initial permutation:";
  gsl_permutation_fprintf(stdout, p.id, " %u");
  std::cout << "\n";

  std::cout << "inverse permutation:";
  gsl::permutation a(N);
  ++++a;
  q = a.inverse();
  gsl_permutation_fprintf(stdout, q.id, " %u");
  std::cout << "\n";
  std::cout << "multiplication:     ";
  gsl_permutation_fprintf(stdout, (a*q).id, " %u");
  std::cout << "\n";
  a = q;

  std::cout << "next permutation:   ";
  q = p;
  gsl_permutation_fprintf(stdout, (++q).id, " %u");
  std::cout << "\n";

  std::cout << "prev permutation:   ";
  ++q;
  gsl_permutation_fprintf(stdout, (--q).id, " %u");
  std::cout << "\n";

  std::cout << "multiplication:     ";
  gsl_permutation_fprintf(stdout, (p*q).id, " %u");
  std::cout << "\n";

  gsl::vector<double> v(N);
  for (int i = 0; i < N; ++i) v[i] = double(i);

  a.permute(v);

  std::cout << "vector permutation: ";
  gsl_vector_fprintf(stdout, v.id, " %g");
  std::cout << "\n";

  return 0;
}
