#include <iostream>
#include "gsl.hpp"

int main()
{
  gsl::vector<double> a(5);

  std::cout << "size = " << a.size() << std::endl;
  a.set(1, -1.2e-3);
  a[2] = 55.1;
  std::cout << "a[1] = " << a.get(1) << std::endl
            << "a[2] = " << a[2] << std::endl
            << "a ptr = " << a.ptr(0) << '\n';
  a[3] = a[2] + 1;
  gsl_vector_fprintf(stdout, a.id, "%g");

  gsl::vector<double> b = a;
  std::cout << "size of b = " << b.size() << std::endl;
  std::cout << "b ptr = " << b.ptr(0) << '\n';
  gsl_vector_fprintf(stdout, b.id, "%g");

  return 0;
}

