#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include "gslevol.hpp"

struct MatElemAccessor {
  const size_t dim;
  inline size_t operator()(size_t i, size_t j) const noexcept {
    return i*dim+j;
  }
};

double norm_inf(const std::vector<double>& v)
{
  double max = 0;
  for (const auto x : v)
    if (max < std::fabs(x)) max = std::abs(x);
  return max;
}

std::vector<double> dif(const std::vector<double>& a, const std::vector<double>& b)
{
  std::vector<double> r(a.size());
  for (size_t i = 0; i < a.size(); ++i) r[i] = b[i] - a[i];
  return r;
}

using namespace gsl::odeiv2;

int main()
{
  static constexpr size_t N = 100;
  double h = 1./double(N-1);
  gsl::odeiv2::system sys{N, [](double t, const double x[], double y[]) {
      y[0] = x[0];
      y[N-1] = x[N-1];
      for (size_t i = 1; i < N-1; ++i)
        y[i] = x[i-1] - 2*x[i] + x[i+1] - 1;
      return GSL_SUCCESS;
    }, [](double t, const double x[], double *dydx, double dydt[]) {
      MatElemAccessor e{N};
      for (size_t i = 0; i < N; ++i)
        for (size_t j = 0; j < N; ++j)
          dydx[e(i,j)] = 0;
      dydx[e(0,0)] = dydx[e(N-1,N-1)] = 1;
      for (size_t i = 1; i < N-1; ++i)
      {
        dydx[e(i,i)] = -2;
        dydx[e(i,i-1)] = dydx[e(i,i+1)] = 1;
      }
      for (size_t i = 0; i < N; ++i)
        dydt[i] = 0;
      return GSL_SUCCESS;
    }
  };
  driver d(sys, RK8PD, 1e-6, 1e-6, 0.0);

  double t = 0.0, t1 = 10000.0;
  std::vector<double> u(N, 0.0), w(N);
  std::ofstream of;

  for (size_t i = 0; i < N; i++)
  {
    const double x = i*h;
    w[i] = .5*x*(x-1)/(h*h);
  }

  for (int i = 1; i <= 100; i++)
  {
    double ti = i * t1 / 100.0;
    int status = d.apply(t, ti, u.data());
    if (status != GSL_SUCCESS)
    {
      std::cout << "error, return value " << status << "\n";
      break;
    }

    std::cout << t << ": " << h*h*norm_inf(dif(u,w)) << "\n";

    of.open("heat.txt");
    if (!of) {std::cout << "could not open file heat.txt\n"; return 2;}
    for (size_t i = 0; i < N; i++)
    {
      const double x = i*h;
      of << x << " " << u[i]*h*h << " " << .5*x*(x-1) << "\n";
    }
    of.close();
    int dummy;
    std::cout << "input any number to continue, -1 to end :"; std::cin >> dummy;
    if (dummy == -1) break;
  }

  return 0;
}
