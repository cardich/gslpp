#include <iostream>
#include "gsl.hpp"

using namespace gsl;

int main()
{
  double a_data[] = { 0.18, 0.60, 0.57, 0.96,
                      0.41, 0.24, 0.99, 0.58,
                      0.14, 0.30, 0.97, 0.66,
                      0.51, 0.13, 0.19, 0.85 };
  double b_data[] = {  1.0,  2.0,  3.0,  4.0 };

  matrix<double> m(4, 4, a_data), A(4, 4);
  vector<double> b(4, b_data), x(4);

  A = m;

  {
    int s;
    permutation p(4);
    vector<double> tau(4), norm(4);

    QRP::decomp(m, tau, p, &s, norm);
    QRP::solve(m, tau, p, b, x);
  }

  std::cout << "x = \n";
  gsl_vector_fprintf(stdout, x.id, "%g");

  vector<double> y(4);
  // gsl_blas_dgemv(CblasNoTrans, 1, A.id, x.id, 0, y.id);
  y = A*x;

  std::cout << "A*x = \n";
  gsl_vector_fprintf(stdout, y.id, "%g");

  return 0;
}
