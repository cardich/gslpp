#include <iostream>
#include "gsl.hpp"

int main()
{
  //gsl_check_range = 0;
  gsl::matrix<double> a(2,3);
  gsl::matrix<float> b(3,3);
  gsl::matrix<int> i(3,3);
  gsl::matrix<unsigned int> ui(3,3);
  gsl::matrix<long> l(2,2);
  gsl::matrix<unsigned long> ul(3,3);
  gsl::matrix<short> s(4,4);
  gsl::matrix<unsigned short> us(3,3);
  gsl::matrix<char> c(2,2);
  gsl::matrix<unsigned char> uc(3,3);
  gsl::matrix<gsl::complex> cd(2,2);
  gsl::matrix<gsl::complex_float> cf(2,3);
  int n;

  std::cout << "a matrix size = " << a.id->size1 << " x " << a.id->size2 << std::endl;
  std::cout << "b matrix size = " << b.id->size1 << " x " << b.id->size2 << std::endl;
  a.set(1, 1, -1.2e-3);
  a(1,2) = 55.1;
  double *p = a.ptr(0,0);
  std::cout << "a(1,1) = " << a.get(1,1) << std::endl
            << "a(1,2) = " << a(1,2) << std::endl
            << "ptr = " << p << '\n';
  a(0,2) = a(1,2) + 1;
  std::cout << "a:" << std::endl;
  gsl_matrix_fprintf(stdout, a.id, "%g");
  b.set_identity();
  std::cout << "b:" << std::endl;
  gsl_matrix_float_fprintf(stdout, b.id, "%g");
  std::cout << "input a number to continue: " << std::endl; std::cin >> n;
  std::cout << "i matrix size = " << i.id->size1 << " x " << i.id->size2 << std::endl;
  std::cout << "ui matrix size = " << ui.id->size1 << " x " << ui.id->size2 << std::endl;
  std::cout << "l matrix size = " << l.id->size1 << " x " << l.id->size2 << std::endl;
  std::cout << "ul matrix size = " << ul.id->size1 << " x " << ul.id->size2 << std::endl;
  i.set_identity();
  std::cout << "i:" << std::endl;
  gsl_matrix_int_fprintf(stdout, i.id, "%d");
  ui.set_all(88);
  std::cout << "ui:" << std::endl;
  gsl_matrix_uint_fprintf(stdout, ui.id, "%u");
  l.set_identity();
  std::cout << "l:" << std::endl;
  gsl_matrix_long_fprintf(stdout, l.id, "%li");
  ul.set_all(7);
  std::cout << "ul:" << std::endl;
  gsl_matrix_ulong_fprintf(stdout, ul.id, "%u");
  std::cout << "input a number to continue: " << std::endl; std::cin >> n;
  std::cout << "s matrix size = " << s.id->size1 << " x " << s.id->size2 << std::endl;
  std::cout << "us matrix size = " << us.id->size1 << " x " << us.id->size2 << std::endl;
  std::cout << "c matrix size = " << c.id->size1 << " x " << c.id->size2 << std::endl;
  std::cout << "uc matrix size = " << uc.id->size1 << " x " << uc.id->size2 << std::endl;
  s.set_identity();
  std::cout << "s:" << std::endl;
  gsl_matrix_short_fprintf(stdout, s.id, "%d");
  us.set_all(9);
  std::cout << "us:" << std::endl;
  gsl_matrix_ushort_fprintf(stdout, us.id, "%u");
  c.set_all('a');
  std::cout << "c:" << std::endl;
  gsl_matrix_char_fprintf(stdout, c.id, "%c");
  uc.set_all('A');
  std::cout << "uc:" << std::endl;
  gsl_matrix_uchar_fprintf(stdout, uc.id, "%c");
  std::cout << "input a number to continue: " << std::endl; std::cin >> n;
  std::cout << "cd matrix size = " << cd.id->size1 << " x " << cd.id->size2 << std::endl;
  std::cout << "cf matrix size = " << cf.id->size1 << " x " << cf.id->size2 << std::endl;
  const gsl::complex tmp1 = {33.,-1.};
  cd.set_all(tmp1);
  cd(1,1) = gsl::complex{.5,-.3};
  std::cout << "cd:" << std::endl;
  gsl_matrix_complex_fprintf(stdout, cd.id, "%g");
  const gsl::complex_float tmp2{11.,-11.};
  cf.set_all(tmp2);
  cf(1,2) = gsl::complex_float{.55,-.33};
  cf(1,0) = cf(1,2);
  std::cout << "cf:" << std::endl;
  gsl_matrix_complex_float_fprintf(stdout, cf.id, "%g");
  std::cout << "cd(1,1): " << cd(1,1).dat[0] << "+(" << cd(1,1).dat[1] << ")*i" << std::endl;
  std::cout << "cf(1,2): " << cf(1,2).dat[0] << "+(" << cf(1,2).dat[1] << ")*i" << std::endl;
  //a.set(7, 3.6);
  return 0;
}
