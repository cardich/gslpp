#include <iostream>
#include "gsl.hpp"

int main()
{
	gsl::matrix<double> a(3,2);
	int n;

	std::cout << "a matrix size = " << a.size1() << " x " << a.size2()
						<< std::endl;
	a(0, 0) = 3.1;
  a(0, 1) = 4.6;
  a(1, 0) = 1.0;
  a(1, 1) = 7.2;
  a(2, 0) = 2.1;
  a(2, 1) = 2.9;

	double x = a(0,1);
	std::cout << "a(0,1) = " << x << std::endl;
	std::cout << "a:" << std::endl;
	std::cout << "a ptr: " << a.ptr(0,0) << std::endl;
	gsl_matrix_fprintf(stdout, a.id, "%g");

	gsl::matrix<double> b = a;
	std::cout << "b:" << std::endl;
	std::cout << "b ptr: " << b.ptr(0,0) << std::endl;
	gsl_matrix_fprintf(stdout, b.id, "%g");

	return 0;
}

