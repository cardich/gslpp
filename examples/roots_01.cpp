#include <cstdio>
#include "gslroot.hpp"

using namespace gsl::root;

int main()
{
  const double a = 1.0, b = 0.0, c = -5.0;

  fdfsolver s(fdfsolver::NEWTON);
  s.set(5., [a,b,c](double x, double *y, double *dy) {
    *y = (a*x + b)*x + c;
    *dy = 2.0*a*x + b;
  });

  printf("using %s method\n", s.name());
  printf("%-5s %10s %10s %10s\n", "iter", "root", "err", "err(est)");

  int status = s.solve(20, [&s](int& status, int iter, double prevsol) {
    static const double r_expected = sqrt(5.0);
    double cursol = s.root();
    status = fdfsolver::test_delta(cursol, prevsol, 0, 1.e-4);
    if (status == GSL_SUCCESS) printf("Converged:\n");
    printf("%5d %10.7f %+10.7f %10.7f\n",
           iter, cursol, cursol - r_expected, cursol - prevsol);
    return status != GSL_SUCCESS;
  });

  return status;
}
