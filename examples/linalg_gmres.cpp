#include <iostream>
#include "gsl.hpp"

using namespace gsl::sp;

int main()
{
  const size_t N = 1000;              // number of grid points
  const size_t n = N - 2;             // subtract 2 to exclude boundaries
  const double h = 1.0 / (N - 1.0);   // grid spacing
  matrix<double> A(n,n);              // triplet format
  matrix<double> C;                   // compressed format
  gsl::vector<double> b(n);           // right hand side vector
  gsl::vector<double> u(n);           // solution vector

  // construct the sparse matrix for the finite difference equation

  // construct first row
  A(0, 0) = -2.0;
  A(0, 1) =  1.0;

  // construct rows [1:n-2]
  for (size_t i = 1; i < n - 1; ++i)
  {
    A(i, i + 1) =  1.0;
    A(i, i    ) = -2.0;
    A(i, i - 1) =  1.0;
  }

  // construct last row
  A(n - 1, n - 1) = -2.0;
  A(n - 1, n - 2) =  1.0;

  // scale by h^2
  gsl_spmatrix_scale(A.id, 1.0 / (h * h));

  // construct right hand side vector
  for (size_t i = 0; i < n; ++i)
    b[i] = -M_PI * M_PI * sin(M_PI * (i + 1) * h);

  // convert to compressed column format
  C = A.ccs();

  // now solve the system with the GMRES iterative solver
  itersolve<double> gmres(n);
  int status = gmres.solve(C, b, u, 5e-9, [&gmres](gsl::vector<double>&, int it) {
    std::cout << "iter " << it << " residual = " << gmres.normr() << "\n";
    return true;
  });
  if (status == GSL_SUCCESS) std::cout << "Converged\n";
  else std::cout << "Did not converge\n";

  // output solution
  for (size_t i = 0; i < n; i+=50)
  {
    const double xi = (i + 1) * h;
    double u_exact = sin(M_PI * xi);
    std::cout << xi << " " << u[i] << " " << u_exact << '\n';
  }

  return 0;
}
