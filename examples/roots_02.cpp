#include <cstdio>
#include "gslroot.hpp"

using namespace gsl::multiroot;
using gsl::vector;

void print_state(int iter, const fsolver& s)
{
  vector<double> x, y;
  x.id = s.id->x;
  y.id = s.id->f;
  printf("iter = %3u x = [% .3f % .3f] f(x) = [% .3e % .3e]\n",
          iter, x[0], x[1], y[0], y[1]);
  x.id = y.id = 0;
}

int main()
{
  int status;
  int iter = 0, max_iter = 9;
  const double a = 1.0, b = 10.0;

  fsolver s(fsolver::HYBRIDS, 2);
  vector<double> x(2);
  x[0] = -10.;
  x[1] =  -5.;

  s.set(x, [a, b](const vector<double>& x, vector<double>& y) {
    y[0] = a * (1 - x[0]);
    y[1] = b * (x[1] - x[0] * x[0]);
    return GSL_SUCCESS;
  });

  print_state(iter, s);

  status = s.solve(max_iter, [&s](int& status, int iter) {
    print_state(iter, s);
    if (status != 0) /* check if solver is stuck */ return false;
    return (status = fsolver::test_residual(s.f(), 1e-7)) == GSL_CONTINUE;
  });

  printf("status = %s\n", gsl_strerror(status));
  printf("status = %s\n", gsl_strerror(GSL_CONTINUE));

  return 0;
}
